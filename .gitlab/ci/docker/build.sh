#!/bin/sh

set -e
set -x

current_dir="$( pwd )"
script_dir="$( cd "$(dirname "$0")"; pwd )"

# Pick up a VTK tag/branch/commit to checkout
# TODO: set VTK_VCS_REF externally with vX.Y.Z for VTK releases.
if [ -z "$VTK_VCS_REF" ]; then
    vtk_vcs_ref="master";
else
    vtk_vcs_ref=$VTK_VCS_REF
fi
readonly vtk_vcs_ref

# Allows compiling custom VTK forks.
if [ -z "$VTK_VCS_URL" ]; then
    vtk_vcs_url="https://gitlab.kitware.com/vtk/vtk.git";
else
    vtk_vcs_url=$VTK_VCS_URL
fi
readonly vtk_vcs_url

# Parse build architecture.
case "$VTK_BUILD_ARCHITECTURE" in
    wasm32)
        enable_64_bit=OFF
        enable_threads=OFF
        ;;
    wasm32-threads)
        enable_64_bit=OFF
        enable_threads=ON
        ;;
    wasm64)
        enable_64_bit=ON
        enable_threads=OFF
        ;;
    wasm64-threads)
        enable_64_bit=ON
        enable_threads=ON
        ;;
    *)
        echo "Unrecognized build architecture $VTK_BUILD_ARCHITECTURE"
        exit 1
        ;;
esac
readonly enable_64_bit
readonly enable_threads

# Install the tools we'll need if we're running in Gitlab CI
if [ "$CI_PROJECT_PATH" = "vtk/vtk-wasm-sdk" ]; then
    dnf install -y git-core podman-docker crun
else
    echo "Assuming you've installed git, podman and crun"
fi

# Clone VTK
[ -d "$script_dir/.vtk" ] || git clone "$vtk_vcs_url" "$script_dir/.vtk"
cd "$script_dir/.vtk"
git checkout "$vtk_vcs_ref"
git submodule update --init --recursive
# Acquire recent release tag and number of commits ahead of that tag.
vtk_version="$(git describe)"
readonly vtk_version
# Setup sccache.
.gitlab/ci/sccache.sh
# Do not need to install cmake and ninja because the dockcross/web-wasm image already has those two.
cd "$current_dir"

# Use podman to avoid having to do docker-in-docker shenanigans.
docker="podman"
readonly docker

# Construct image tags from vtk_version and date.
# Ex: kitware/vtk-wasm-sdk:vX.Y.Z-<commits_ahead>-g<abcdefghij>-YYYYMMDD <- Built with VTK master/other branch or a specific commit.
#     kitware/vtk-wasm-sdk:vX.Y.Z-YYYYMMDD <- Built with VTK vX.Y.Z
#     kitware/vtk-wasm-sdk:latest <- Always points to most recent image.
image_dated_tag="$vtk_version-$( date "+%Y%m%d" )"
readonly image_dated_tag

image_tag="$VTK_BUILD_ARCHITECTURE-$image_dated_tag"
readonly image_tag

# Build the new image.
$docker build --format=docker \
    --cgroup-manager=cgroupfs \
    --volume "$script_dir/.vtk:/VTK-src:Z" \
    "--build-arg=BUILD_DATE=$(date)" \
    "--build-arg=SCCACHE_REDIS=$SCCACHE_REDIS" \
    "--build-arg=IMAGE_TAG=$image_tag" \
    "--build-arg=REVISION=$vtk_vcs_ref" \
    "--build-arg=SOURCE_URL=$vtk_vcs_url" \
    "--build-arg=VERSION=$vtk_version" \
    "--build-arg=ENABLE_64_BIT=$enable_64_bit" \
    "--build-arg=ENABLE_THREADS=$enable_threads" \
    -t "kitware/vtk-wasm-sdk:$image_tag" \
    "$script_dir" "$@" \
    2>&1 | tee "$script_dir/build.log"

$docker save --format=docker-archive -o "$script_dir/vtk-wasm-sdk-emscripten.tar" "kitware/vtk-wasm-sdk:$image_tag"
echo "$image_tag" > "$script_dir/TAG"
