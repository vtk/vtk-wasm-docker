#!/bin/sh

set -e
set -x

script_dir="$( cd "$(dirname "$0")"; pwd )"

# Install the tools we'll need if we're running in Gitlab CI
if [ "$CI_PROJECT_PATH" = "vtk/vtk-wasm-sdk" ]; then
    dnf install -y podman-docker crun
else
    echo "Assuming you've installed podman and crun"
fi

# Push the images to DockerHub.
image_build_tag="$( cat "$script_dir/TAG" )"
readonly image_build_tag

# Use podman to avoid having to do docker-in-docker shenanigans.
docker="podman"
readonly docker

$docker image load -i "$script_dir/vtk-wasm-sdk-emscripten.tar"

if [ "$VTK_IMAGE_TAG" = "latest" ]; then
    $docker tag "kitware/vtk-wasm-sdk:$image_build_tag" "kitware/vtk-wasm-sdk:latest"
fi

set +x
echo "Logging into index.docker.io ..."
$docker login --username "$DOCKERHUB_USERNAME" --password "$DOCKERHUB_PASSWORD" "index.docker.io"
set -x
$docker push "kitware/vtk-wasm-sdk:$image_build_tag"
if [ "$VTK_IMAGE_TAG" = "latest" ]; then
    $docker push "kitware/vtk-wasm-sdk:latest"
fi
