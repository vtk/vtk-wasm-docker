#!/bin/sh

set -e
set -x

script_dir="$( cd "$(dirname "$0")"; pwd )"

image_tag="$( cat "$script_dir/TAG" )"
readonly image_tag

# Install the tools we'll need if we're running in Gitlab CI
if [ "$CI_PROJECT_PATH" = "vtk/vtk-wasm-sdk" ]; then
    dnf install -y podman-docker crun
else
    echo "Assuming you've installed git, podman and crun"
fi

# Use podman to avoid having to do docker-in-docker shenanigans.
docker="podman"
readonly docker

$docker image load -i "$script_dir/vtk-wasm-sdk-emscripten.tar"

# Parse build architecture and configure with VTK for the appropriate architecture.
case "$VTK_BUILD_ARCHITECTURE" in
    wasm32)
        $docker run --rm -it \
        -v"$script_dir/tests/basic":/work/basic \
        "kitware/vtk-wasm-sdk:$image_tag" \
        emcmake cmake \
        -GNinja -S basic \
        -B basic/out \
        -DCMAKE_BUILD_TYPE=Release \
        -DVTK_DIR=/VTK-install/Release/lib/cmake/vtk
        ;;
    wasm32-threads)
        $docker run --rm -it \
        -v"$script_dir/tests/basic":/work/basic \
        "kitware/vtk-wasm-sdk:$image_tag" \
        emcmake cmake \
        -DENABLE_THREADS=ON \
        -GNinja -S basic \
        -B basic/out \
        -DCMAKE_BUILD_TYPE=Release \
        -DVTK_DIR=/VTK-install/Release/lib/cmake/vtk
        ;;
    wasm64)
        $docker run --rm -it \
        -v"$script_dir/tests/basic":/work/basic \
        "kitware/vtk-wasm-sdk:$image_tag" \
        emcmake cmake \
        -DCMAKE_C_FLAGS="-sMEMORY64=1" \
        -GNinja -S basic \
        -B basic/out \
        -DCMAKE_BUILD_TYPE=Release \
        -DVTK_DIR=/VTK-install/Release/lib/cmake/vtk
        ;;
    wasm64-threads)
        $docker run --rm -it \
        -v"$script_dir/tests/basic":/work/basic \
        "kitware/vtk-wasm-sdk:$image_tag" \
        emcmake cmake \
        -DCMAKE_C_FLAGS="-sMEMORY64=1" \
        -DENABLE_THREADS=ON \
        -GNinja -S basic \
        -B basic/out \
        -DCMAKE_BUILD_TYPE=Release \
        -DVTK_DIR=/VTK-install/Release/lib/cmake/vtk
        ;;
    *)
        echo "Unrecognized build architecture $VTK_BUILD_ARCHITECTURE"
        exit 1
        ;;
esac

# Build the unit tests
$docker run --rm -it -v"$script_dir/tests/basic":/work/basic "kitware/vtk-wasm-sdk:$image_tag" cmake --build basic/out

# Run the unit tests
$docker run --rm -it -v"$script_dir/tests/basic":/work/basic "kitware/vtk-wasm-sdk:$image_tag" ctest --extra-verbose --test-dir basic/out
