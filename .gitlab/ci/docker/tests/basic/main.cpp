/**
 * Simple C++ program which exercises basic pointer size, creating and joining
 * threads.
 */

#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>

#include <vtkSmartPointer.h>
#include <vtkTypeUInt32Array.h>

auto main(int argc, char *argv[]) -> int {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <wordsize> <number of threads>\n";
    return 1;
  }
  // Test pointer size matches 32/64
  std::size_t wordsize = std::atoi(argv[1]);
  bool wordSizeTestPassed = false;
  if (wordsize == 32) {
    if (sizeof(void *) != 4) {
      std::cerr << "Test failed for word size=32\n";
      return EXIT_FAILURE;
    }
  } else if (wordsize == 64) {
    if (sizeof(void *) != 8) {

      std::cerr << "Test failed for word size=64\n";
      return EXIT_FAILURE;
    }
  } else {
    std::cerr << "Word size is unsupported " << wordsize << '\n';
    return EXIT_FAILURE;
  }

  std::size_t numThreads = std::atoi(argv[2]);
  if (numThreads > 0) {
    auto sharedData = vtk::TakeSmartPointer(vtkTypeUInt32Array::New());
    sharedData->SetNumberOfValues(numThreads);
    std::vector<std::unique_ptr<std::thread>> threads(numThreads);
    // write 2 * threadId into sharedData[threadId] from the thread.
    for (std::size_t threadId = 0; threadId < numThreads; ++threadId) {
      auto &thread = threads[threadId];
      thread.reset(new std::thread([threadId, sharedData]() {
        sharedData->SetTypedComponent(threadId, 0, 2 * threadId);
      }));
    }
    for (const auto &thread : threads) {
      thread->join();
    }
    // verify sharedData is updated.
    for (std::size_t threadId = 0; threadId < numThreads; ++threadId) {
      const auto value = sharedData->GetTypedComponent(threadId, 0);
      if (value != 2 * threadId) {
        std::cerr << "Incorrect value=" << value
                  << " != 2 * threadId at threadId=" << threadId << '\n';
        return EXIT_FAILURE;
      }
    }
  }

  return EXIT_SUCCESS;
}
