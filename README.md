# VTK WebAssembly SDK Docker Image

This dockerfile builds a self-contained version of VTK C++ so that CMake projects that link with VTK can target WebAssembly in or outside browsers without requiring one to compile VTK from source.

## Usage

Let's work with an existing C++ example from VTK repository.

1. Download the source code from
[here](https://gitlab.kitware.com/vtk/vtk/-/blob/master/Examples/Emscripten/Cxx/Cone/Cone.cxx)
    ```shell
    $ curl -L --output Cone.zip https://gitlab.kitware.com/vtk/vtk/-/archive/master/vtk-master.zip?path=Examples/Emscripten/Cxx/Cone
    $ unzip -j Cone.zip -d Cone
    ## You should now see these files in Cone directory
    $ ls Cone
      CMakeLists.txt  Cone.cxx  index.html  README.md
    ```
2. Configure the CMake project

    ```shell
    $ docker run \
      --rm \
      -u $(id -u):$(id -g) \
      -v $(pwd)/Cone:/Cone \
      kitware/vtk-wasm-sdk \
      emcmake cmake -S /Cone -B /Cone/build -DCMAKE_BUILD_TYPE=Release -DVTK_DIR=/VTK-install/Release/lib/cmake/vtk
    ```

3. Build

    ```shell
    $ docker run \
      --rm \
      -u $(id -u):$(id -g) \
      -v $(pwd)/Cone:/Cone \
      kitware/vtk-wasm-sdk \
      cmake --build /Cone/build
    ```


4. Run
    ```shell
    $ python3 -m http.server -d ./Cone/build 8080
    ```
    Now, go to http://localhost:8080 in a web browser.

Teardown of configure command:

|part|description|
|:--:|:---------:|
|`docker run`|A standard command to run a command in a container|
|`--rm`|remove a container after execution|
|`-u $(id -u):$(id -g)`|Run the container as a non-root user with the same UID and GID as local user. Hence all files produced by this are accessible to non-root users|
|`-v $(pwd)/Cone:/Cone`|Mounting `Cone` from the current folder in the host system into mirrored path on the container
TIP: This helps to investigate possible problem as we preserve exactly the same paths like in host. In such case modern editors (like Sublime, Atom, VS Code) let us to CTRL+Click on a problematic file|
|`kitware/vtk-wasm-sdk`|Get the latest tag of this container|
|`emcmake cmake ...`|Execute `emcmake` command with following arguments inside container, effectively configure our CMake project|

## Building Dockerfile

This image optionally accepts the following build arguments:

|arg|description|
|:-:|:---------:|
|`BUILD_DATE`|Date stamp for container metadata|
|`SCCACHE_REDIS`|`sccache` redis url|
|`IMAGE_NAME`|image name for container metadata|
|`IMAGE_TAG`|image tag for container metadata|
|`REVISION`|git revision for container metadata|
|`SOURCE_URL`|VTK source url for container metadata|
|`VERSION`|VTK version for container metadata|

### Building

This step will build Dockerfile as given tag on local machine

```shell
# using docker
$ docker build \
  --network host \
  --build-arg=BUILD_DATE=$(date) \
  --build-arg=VERSION=9.3.0 \
  --tag kitware/vtk-wasm-sdk:v9.3.0 \
  .
```

### Tagging

In case of using docker build command directly, given `--tag` should match version of released VTK.

### Pushing

This step will take local image and push to default docker registry. You need to make sure that you logged in docker cli (`docker login`) and you have rights to push to that registry.

```shell
# using docker
$ docker push kitware/vtk-wasm-sdk:v9.3.0
```

In case of pushing the most recent version, this version should be also tagged as `latest` and pushed.

```shell
# using docker cli
docker tag kitware/vtk-wasm-sdk:v9.3.0 kitware/vtk-wasm-sdk:latest
docker push kitware/vtk-wasm-sdk:latest
```

## Extending

If your project uses packages that this image doesn't provide you might want to:

- Contribute to this repo: Maybe your dependency is either non-intrusive or could be useful for other people
- Create custom image that bases on this image
  1. create own Dockerfile that holds:

    ```Dockerfile
    # Point at any base image that you find suitable to extend.
    FROM kitware/vtk-wasm-sdk:v9.3.0

    # Install required tools that are useful for your project i.e. ninja-build
    RUN apt update && apt install -y gfortran
    ```
  2. build it

    ```shell
    docker build -t extended_vtk_wasm_sdk .
    ```

  3. test

    ```shell
    docker run --rm extended_vtk_wasm_sdk gfortran --version
    # GNU Fortran (Ubuntu 9.4.0-1ubuntu1~20.04.2) 9.4.
    ```

## Advanced usage (webpack integration)

In this guide, let's go through steps to integrate VTK-wasm with WebPack and npm scripts. Here's what the directory structure can look like:

```bash
src
|-App.cpp # C++ code for VTK-wasm application
|-App.h # C++ code for VTK-wasm application (does not expose VTK objects)
|-main.cpp # Optional, no wasm, runs the application natively.
|-CMakeLists.txt # Logic to build App, knows nothing about web/
web
|-index.html
|-index.js # Initialize wasm module, loads App from C++ land, does web stuff
package.json # Metadata and script hooks to build the project for web.
webpack.config.js # Logic to build and serve webpage
```

First, install [itk-wasm](https://wasm.itk.org) and save it in `package.json`
```
$ npm install --save-dev itk-wasm@1.0.0-b.83
```

Now, let's teach `package.json` the way to build our C++ code with [kitware/vtk-wasm-sdk](https://hub.docker.com/r/kitware/vtk-wasm-sdk) and [itk-wasm](https://wasm.itk.org) CLI.

```json
"name": ...
"version": ...
"description": ...
"main": "index.js"
"scripts": {
    "build-wasm": "npm run build-wasm:release",
    "build-wasm:release": "npx itk-wasm -i kitware/vtk-wasm-sdk -b build-emscripten -s src build -- Release -DDEBUGINFO=NONE",
    "build-wasm:profile": "npx itk-wasm -i kitware/vtk-wasm-sdk -b build-emscripten -s src build -- Release -DDEBUGINFO=PROFILE",
    "build-wasm:debug": "npx itk-wasm -i kitware/vtk-wasm-sdk -b build-emscripten -s src build -- Debug -DDEBUGINFO=DEBUG_NATIVE -DOPTIMIZE=NO_OPTIMIZATION",
}
```

After the C++ code is built, the `*.wasm` and `*.js` glue code sit in `src/build-emscripten`. Let's teach `webpack.config.js` to bundle and serve these files along with contents from `web/`.

```js
plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.join(
            __dirname, "src", "build-emscripten", "NameOfYourWasmApp.js"
          ),
          to: path.join(__dirname, "dist", "NameOfYourWasmApp.js")
        },
        {
          from: path.join(
            __dirname, "src", "build-emscripten", "NameOfYourWasmApp.wasm"
          ),
          to: path.join(__dirname, "dist", "NameOfYourWasmApp.wasm")
        }
      ],
    })
]
```

[vtkWasmBenchmark](https://github.com/jspanchu/vtkWasmBenchmark) is a complete example of webpack integration with [kitware/vtk-wasm-sdk](https://hub.docker.com/r/kitware/vtk-wasm-sdk) and [itk-wasm](https://wasm.itk.org) CLI.
